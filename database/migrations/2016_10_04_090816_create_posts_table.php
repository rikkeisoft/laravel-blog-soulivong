<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    // when brign up
    public function up()
    {
        // create table
        Schema::create('posts', function(Blueprint $table) {
            // define schema 
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->text('body');
            $table->string('user_id')->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    // when bring down
    public function down()
    {
        // delete table
        Schema::drop('posts');
    }
}
