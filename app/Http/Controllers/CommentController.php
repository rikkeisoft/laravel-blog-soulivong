<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Models\Comment;

class CommentController extends Controller
{
    // currently no functions used in this controller
    
    public function store($id, $commentContent)
    {
        if (auth()->check()) {
            Comment::create([
                'post_id' => $id,
                'body' => $commentContent,
                'user_id' => auth()->user()->id
            ]);
            return 'true';
        }else{
            return 'false';
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
