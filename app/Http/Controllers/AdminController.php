<?php
namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Requests;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        // list all users 
        $users = User::all();

        // list all posts
        $posts = Post::all();

        return view('admin/index', compact('users', 'posts'));
    }
}
