<?php

namespace App\Http\Controllers;

use App\Models\Comment;

use Illuminate\Http\Request;

use App\Http\Requests;

class PostCommentController extends Controller
{
    public function destroy($postId, $commId)
    {
        Comment::find($commId)->delete();
        return redirect('posts/' . $postId);
    }
}
