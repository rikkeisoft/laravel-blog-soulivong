<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;

class PostController extends Controller
{

    public function __construct()
    {
        // only admin can access
//        $this->middleware('admin', ['only' => ['update', 'edit', 'destroy']]);
        // only signed in user can access which is covered by admin

        $this->middleware('user_access', ['only' => ['store', 'edit', 'update', 'destroy']]);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
                'title' => 'required|max:255'
        ]);
    }

//    public function test(){
//        echo "check middleware";
//    }
    // display a list of posts
    public function index()
    {

//        $posts = Post::all();

        $admin = $this->checkAdmin();

        // pagination
        $posts = Post::orderBy('created_at', 'desc')->paginate(2);

        // compact() array containing value
        return view('index', compact('posts', 'users', 'admin', 'pages'));
    }

    // display creating post form
    public function create()
    {
        // only signed in user can access

        return view('posts.create');
    }

    // save a newly created post
    public function store(Request $request)
    {
        // only signed in user can access

        $this->validate($request, [
            'title' => 'required|unique:posts|max:255',
            'body' => 'required'
        ]);
        Post::create([
            'title' => $request->input('title'),
            'body' => $request->input('body'),
            'user_id' => Auth::user()->id
        ]);
        return redirect('/posts');
    }

    // display a specific post
    public function show($id)
    {
        $post = Post::find($id);

        $admin = $this->checkAdmin();

        $author = $post->user->name;

//        $postComm = Comment::where('post_id', '=', $post->id)->get();
        $postComm = $post->comments;

        return view('posts.show', compact('post', 'postComm', 'admin', 'author'));
    }

    // display a form for editing a specific post
    public function edit($id)
    {
        // only admin can access

        $post = Post::find($id);
        return view('posts.edit', compact('post'));
    }

    // update a specific post after edit
    public function update(Request $request, $id)
    {
        // only admin can access

        $post = Post::find($id);
        $post->update([
            'title' => $request->input('title'),
            'body' => $request->input('body'),
            'user_id' => Auth::user()->id
        ]);

        return redirect('/posts');
    }

    // delete a specific post
    public function destroy($id)
    {
        // only admin can access

        $this->middleware('admin');

        Post::find($id)->delete();
        return redirect('posts');
    }

    protected function checkAdmin()
    {
        if (auth()->check()) {
            if (auth()->user()->isAdmin == true) {
                return true;
            }
        }
        return false;
    }
}
