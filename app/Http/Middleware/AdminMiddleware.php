<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // user have not logged in
        if (!Auth::guard($guard)->check()) {
            return redirect('/posts');
        } else {
            // logged in
            if (Auth()->user()->isAdmin != 1) {
                return redirect('/posts');
            }
        }
        return $next($request);
    }
}
