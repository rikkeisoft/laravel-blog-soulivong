<?php
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//Route::get('/', function () {
//     return view('welcome');
//});

Route::get('/', 'PostController@index');

Route::get('/blog', function() {
    return view('blog-home');
});

//Route::get('/posts/test', function() {
//    return view('blog-home');
//})->middleware('admin');


Route::resource('posts', 'PostController');

Route::resource('posts.comments', 'PostCommentController');

Route::resource('comments', 'CommentController');

Route::resource('users', 'UserController');

Route::resource('admin', 'AdminController');

// authentication routes
//Route::get('auth/login', 'Auth\AuthController@getLogin');
//Route::post('auth/login', 'Auth\AuthController@postLogin');
//Route::get('auth/logout', 'Auth\AuthController@getLogout');
// registration routes
//Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'Auth\AuthController@postRegister');
//
//Route::controllers([
//    'password' => 'Auth\PasswordController'
//]);
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/comments/create/{id}/{str}/', 'CommentController@store');

Route::post('/users/fileUpload/', 'UserController@fileUpload');

