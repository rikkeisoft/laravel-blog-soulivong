@extends('layouts.app')

@section('content')

<h1 style='color: brown'>Users</h1>
<table class='table table-hover'>
    <thead>
        <tr>
            <th>No</th>
            <th>User ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Register Date</th>
        </tr>
    </thead>
    <tbody>
        <?php $counter = 1; ?>
        @foreach($users as $user)
        <tr>
            <td><?php echo $counter; ?></td>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->created_at }}</td>
        </tr>
        <?php $counter++; ?>
        @endforeach
        <tr>
            <td colspan="5"><strong>Total: <?php echo $counter - 1; ?> users</strong></td>
        </tr>
    </tbody>
</table>
<br><br>
<h1 style='color: brown'>Posts</h1>
<table class='table table-hover'>
    <thead>
        <tr>
            <th>No</th>
            <th>Post ID</th>
            <th>Title</th>
            <th>Author</th>
            <th>Create</th>
            <th>Latest Update</th>
        </tr>
    </thead>
    <tbody>
        <?php $counter = 1; ?>
        @foreach($posts as $post)
        <tr>
            <td><?php echo $counter; ?></td>
            <td>{{ $post->id }}</td>
            <td>{{ $post->title }}</td>
            <td>{{ $post->user->name }}</td>
            <td>{{ $post->created_at }}</td>
            <td>{{ $post->updated_at }}</td>
        </tr>
        <?php $counter++; ?>
        @endforeach
        <tr>
            <td colspan="7"><strong>Total: <?php echo $counter - 1; ?> posts</strong></td>
        </tr>
    </tbody>
</table>

@endsection