@extends('layouts.app')

@section('content')

<div class='container'>
    <style>
        #btnDelete{
            padding: 0px 0px 0px 0px
        }
        #pagination{
            text-align: center
        }
    </style>

    <div class='row'>
        @foreach($posts as $post)
        <div class="card">
            <div class="card-header">
                <h2>
                    <a href='{{ url('/posts', $post->id)}}'>
                        @if($admin)
                        {{$post->id}} | 
                        @endif 
                        {{$post->title}}
                    </a>

                </h2>
                <p>Created: <small>{{$post->created_at}}</small></p>
            </div>
            <div class="card-block">
                <p class="card-text">{{$post->body}}</p>
                <p><small>Author: 
                        <cite>{{$post->user->name}}</cite>
                    </small>
                </p>
                <p>Editted: <small>{{$post->updated_at}}</small></p>
                @if($admin)
                {!! Form::open(['method' => 'DELETE', 'route'=>['posts.destroy', $post->id], 'onsubmit' => 'return confirmDelete()']) !!}
                <a href='{{ route('posts.edit', $post->id)}}' class='btn btn-sm btn-warning'>edit</a>
                {!! Form::submit('delete', ['class' => 'btn btn-sm btn-danger']) !!}
                {!! Form::close() !!}
                @elseif(auth()->check() && auth()->user()->id == $post->user_id)
                {!! Form::open(['method' => 'DELETE', 'route'=>['posts.destroy', $post->id], 'onsubmit' => 'return confirmDelete()']) !!}
                <a href='{{ route('posts.edit', $post->id)}}'>edit</a> |
                {!! Form::submit('delete', ['class' => 'btn btn-link', 'id' => 'btnDelete']) !!}
                {!! Form::close() !!}
                @endif
            </div>
        </div>
        <br>
        @endforeach
        <div id='pagination'>
            {{ $posts->links() }}
        </div>
    </div>
</div>
<script>
    function confirmDelete() {
        var x = confirm('Are you sure you want to delete this post?');
        if (x) {
            return true;
        } else {
            return false;
        }
    }
</script>
@endsection