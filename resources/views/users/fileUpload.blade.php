<style>
    #label {
        pointer: cursor;
        color: #58af8c;
        /* Style as you please, it will become the visible UI component. */
    }

    #imgInp {
        opacity: 0;
        position: absolute;
        z-index: -1;
    }
</style>
@if(count($errors)>0)
<div class='alert alert-danger'>
    <strong>Whoops!</strong> There are some problems with your input. <br><br>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

{!! Form::open(['url' => 'users/fileUpload', 'enctype' => 'multipart/form-data']) !!}
<div class='row cancel'>
    <div class='col-md-4'>
        <label id='label'>change
            {!! Form::file('image', array('class' => 'image', 'id' => 'imgInp')) !!}
        </label>
        <img id="blah" src="#"  />
        <div class='col-md-4'>
            <button type='submit' class='btn btn-xs'>Save</button>
        </div>
    </div>
</div>
{!! Form::close() !!}
