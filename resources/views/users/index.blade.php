@extends('layouts.app')

@section('content')

<style>

    .table-user-information > tbody > tr {
        border-top: 1px solid rgb(221, 221, 221);
    }

    .table-user-information > tbody > tr:first-child {
        border-top: 0;
    }


    .table-user-information > tbody > tr > td {
        border-top: 0;
    }
    .toppad
    {
        margin-top:20px;
    }

</style>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-2 col-lg-offset-2 toppad">


        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{ $user->name }}</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2 " align="center"> 
                        <img alt="User Pic" src="{{ url('/') }}/img/user_profile.jpg" class="img-circle img-responsive"> 
                        @include('users.fileUpload')
                    </div>

                    <div class=" col-md-9 col-lg-9 ">       
                        <table class="table table-user-information">
                            <tbody>
                                <tr>
                                    <td>Id</td>
                                    <td>{{ $user->id }}</td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td><a href='mailto:{{ $user->email }}'>{{ $user->email }}</a></td>
                                </tr>

                                <tr>
                                    <td>Registered</td>
                                    <td>{{ $user->created_at }}</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#imgInp").change(function () {
        readURL(this);
    });
</script>
@stop