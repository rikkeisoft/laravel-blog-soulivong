@extends('layouts/app')
@section('content')
<div class='row'>
    <div class="card">
        <div class="card-header">
            <h2>
                <a href='{{ url('/posts', $post->id)}}'>
                    @if($admin)
                    {{$post->id}} | 
                    @endif 
                    {{$post->title}}
                </a>

            </h2>
            <p>Created: <small>{{$post->created_at}}</small></p>
        </div>
        <div class="card-block">
            <p class="card-text">{{$post->body}}</p>
            <p><small>Author: <cite>{{ $author }}</cite></small></p>
            <p>Editted: <small>{{$post->updated_at}}</small></p>
            @if($admin)
            {!! Form::open(['method' => 'DELETE', 'route'=>['posts.destroy', $post->id], 'onsubmit' => 'return confirmDelete()']) !!}
            <a href='{{ route('posts.edit', $post->id)}}' class='btn btn-sm btn-warning'>edit</a>
            {!! Form::submit('delete', ['class' => 'btn btn-sm btn-danger']) !!}
            {!! Form::close() !!}
            @elseif(auth()->check() && auth()->user()->id == $post->user_id)
            {!! Form::open(['method' => 'DELETE', 'route'=>['posts.destroy', $post->id], 'onsubmit' => 'return confirmDelete()']) !!}
            <a href='{{ route('posts.edit', $post->id) }}' class='btn btn-sm btn-warning' />edit</a> 
            {!! Form::submit('delete', ['class' => 'btn btn-sm btn-danger']) !!}
            {!! Form::close() !!}
            @endif
        </div>
    </div>
    <hr>
    <div id="commentBox" class="col-sm-6">
        @foreach($postComm as $comm)
        <div class="card">
            @if($admin)
            {!! Form::open(['method' => 'DELETE', 'route'=>['posts.comments.destroy', $post->id, $comm->id]]) !!}
            {!! Form::submit('x', ['class' => 'btn btn-link pull-right']) !!}
            @elseif(auth()->check() && $comm->user->id == auth()->user()->id)
            {!! Form::open(['method' => 'DELETE', 'route'=>['posts.comments.destroy', $post->id, $comm->id]]) !!}
            {!! Form::submit('x', ['class' => 'btn btn-link pull-right']) !!}
            @endif
            <div class="card-block">
                <small><i style="color: orange">{{ $comm->user->name }}: </i></small><p class="card-text">{{ $comm->body }}</p>
            </div>
            {!! Form::close() !!}
            {{ Form::hidden('postId', $post->id, ['class' => 'form-control']) }}
        </div>
        @endforeach
    </div>
</div>
<div class="row">
    <div>
        <form>
            <h1>Leave a comment</h1>
            <div class='form-group'>
                <input hidden id='txtPostId' value='{{$post->id}}'/>
                <textarea rows='4' class='form-control' name="txtComment" id='txtComment'></textarea>
            </div>
            <input id="cmmt" type="button" class='btn btn-primary' value="Comment" onclick="createComments();" disabled />
            @if (!auth()->check())
            <span style="color:red">please login to comment</span>
            @endif
        </form>
    </div>  
    
    <h1>Comment With Facebook</h1>
    <div class="fb-comments" data-href="http://localhost:8000/posts/{{$post->id}}" data-numposts="5"></div>
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    
    <hr>
    <a href='{{ url('posts') }}'>back</a>
    <br><br><br>
</div>
<script type="text/javascript">
    $(function () {
        var isValid = <?php echo auth()->check() ?>;
        if (isValid == 1) {
            $('#cmmt').removeAttr('disabled');
        }
    });

    function createComments() {
        var username = "<?php
if (auth()->check()) {
    echo Auth()->user()->name;
}

?>";
        $.ajax({
            url: '/comments/create/' + $('#txtPostId').val() + '/' + $('#txtComment').val(),
            type: 'GET',
            success: function (data) {
                if (data == 'true') {
                    var divComment = "<div class='card'>"
                            + "<button class='btn btn-link pull-right' value='x'/>"
                            + "<div class='card-block'><small>"
                            + "<i style='color: orange'>"
                            + username
                            + ": </i></small><p class='card-text'>"
                            + document.getElementById('txtComment').value
                            + "</p></div>"
                    $('#commentBox').append(divComment);
                    document.getElementById('txtComment').value = "";
                }
            }
        });
    }
</script>
@stop
