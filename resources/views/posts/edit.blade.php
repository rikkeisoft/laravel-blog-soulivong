@extends('layouts.app')
@section('content')
<div class='row'>
    <h1>Update Post</h1>
    {!! Form::model($post, ['method' => 'PATCH', 'route' => ['posts.update', $post->id]]) !!}
    <div class='form-group'>
        {{ Form::label('title', 'Title:') }}
        {{ Form::text('title', null, ['class' => 'form-control']) }}
    </div>
    <div class='form-group'>
        {{ Form::label('body', 'Body:') }}
        {{ Form::textarea('body', null, ['class' => 'form-control']) }}
    </div>
    <div>
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{{ URL::previous() }}" class="btn btn-default">Cancel</a>
    </div>
    {!! Form::close() !!}
</div>
@stop