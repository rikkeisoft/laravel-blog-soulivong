@extends('layouts.app')

@section('content')

<div class='row'>
    <h1>Create A New Post</h1>
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    {!! Form::open(['url' => 'posts']) !!}
    <div class='form-group'>
        {{ Form::label('title', 'Title:') }}
        {{ Form::text('title', null, ['class' => 'form-control']) }}
    </div>
    <div class='form-group'>
        {{ Form::label('body', 'Body:') }}
        {{ Form::textarea('body', null, ['class' => 'form-control']) }}
    </div>
    <div>
        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        <a href="{{ URL::previous() }}" class="btn btn-default">Cancel</a>
        @if (!auth()->check())
        <span style="color:red">please login/sign up to create a post</span>
        @endif
    </div>
    {!! Form::close() !!}
</div>
@endsection 